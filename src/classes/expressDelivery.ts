export class ExpressDelivery {

    // Reprend la méthode de l'interface Deliverable
    estimateDeliveryTime(weight : number) : number {
        let days : number = 0;
        weight < 5 ? days = 1 : days = 3;
        return days
    }

    // Reprend la méthode de calcul des frais de livraison
    calculateShippingFee(weight: number): number {
        let fee : number = 0
        if (weight < 1) {
            fee = 8
        } else if ( weight > 1 && weight < 5 ) {
            fee = 14
        } else {
            fee = 30
        }
        return fee
    }
}