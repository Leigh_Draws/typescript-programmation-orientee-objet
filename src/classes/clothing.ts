import { ClothingSize } from "../enum/enum";
import { Dimensions, Product } from "./product";

export class Clothing extends Product {
    size : ClothingSize;

    constructor ( productId : number, name : string, weight : number, price : number, dimension : Dimensions, size : ClothingSize) {
        super( productId, name, weight, price, dimension );
        this.size = size
    }

    displayDetails() : string {
        return `${super.displayDetails()}Size: ${this.size} `
    }
} 