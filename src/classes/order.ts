import { Deliverable } from "../interface/deliverable";
import { Customer } from "./customer";
import { Product } from "./product";

// Création d'un type custom pour Date
export type OrderDate = {
    day: number;
    month: number;
    year: number
}

// Création de la classe Order
export class Order {
    orderId: number;
    customer: Customer;
    productList: Product[];
    orderDate: OrderDate;
    delivery : Deliverable | undefined;

    constructor(orderId: number, customer: Customer, productList: Product[], orderDate: OrderDate) {
        this.orderId = orderId;
        this.customer = customer;
        this.productList = productList;
        this.orderDate = orderDate
    };

    // Méthode pour ajouter un élément au tableau Product
    addProduct(product: Product) {
        this.productList.push(product)
    };

    // Méthode pour enlever un élément au tableau Product
    removeProduct(product: Product) {
        const indexToRemove = this.productList.indexOf(product);
        let removed = this.productList.splice(indexToRemove, 1)
    };

    // Méthode de calcul du poid total de la commande
    calculateWeight(): number {
        let totalWeight: number = 0;
        for (let eachProduct of this.productList) {
            totalWeight += eachProduct.weight
        }
        return totalWeight
    }

    // Méthode de calcul du prix de la commande
    calculateTotal(): number {
        let totalPrice: number = 0;
        for (let eachProduct of this.productList) {
            totalPrice += eachProduct.price
        }
        return totalPrice
    }

    // Méthode qui affiche les détails de la commande
    displayOrder(): string {
        let productListString: string = '';
        for (let eachProduct of this.productList) {
            productListString += `${eachProduct.name}, `
        }
        return `
        Order number : ${this.orderId}\n
        Customer informations : ${this.customer.displayInfo()}
        Products : ${productListString}\n
        Date of the order : ${this.orderDate.day}/${this.orderDate.month}/${this.orderDate.year}
        Total : ${this.calculateTotal()} €\n`

    }

    setDelivery(delivery : Deliverable) {
        this.delivery = delivery
    }

    // Méthode de calcul des frais de ports
    calculateShippingCost() {
        const totalWeight = this.calculateWeight()
        return this.delivery?.calculateShippingFee(totalWeight)
    }

    // Méthode de calcul du temps de livraison
    estimateDeliveryTime(weight : number) {
        const totalWeight = this.calculateWeight() 
        return this.delivery?.estimateDeliveryTime(totalWeight)
    }

}