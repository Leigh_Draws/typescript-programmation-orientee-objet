import { ShoeSize } from "../enum/enum";
import { Dimensions, Product } from "./product";

export class Shoes extends Product {
    size : ShoeSize;

    constructor ( productId : number, name : string, weight : number, price : number, dimension : Dimensions, size : ShoeSize) {
        super( productId, name, weight, price, dimension );
        this.size = size
    }

    displayDetails() : string {
        return `${super.displayDetails()}Size: ${this.size} `
    }
}