export type Dimensions = {
    length : number;
    width : number;
    height : number
}

export class Product {
    productId : number;
    name : string;
    weight : number;
    price : number;
    dimension : Dimensions

    constructor(productId : number, name : string, weight : number, price : number, dimension : Dimensions) {
        this.productId = productId;
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.dimension = dimension
    }

    displayDetails(): string {
        return `Product ID: ${this.productId}\nName: ${this.name}\nWeight: ${this.weight} kg\nPrice: ${this.price} €\nDimension: ${this.dimension.length} ${this.dimension.width} ${this.dimension.height} \n`
    }
}