export type Address = {
    street : string;
    city : string;
    postalCode : string;
    country : string
};

export class Customer {
    customerId : number;
    name : string;
    email : string;
    address : Address | undefined

    constructor(customerId : number, name : string, email : string) {
        this.customerId = customerId;
        this.name = name;
        this.email = email
    }

    displayInfo(): string {
        return `Customer ID: ${this.customerId}\n
        Name: ${this.name}\n
        Email: ${this.email}\n
        ${this.displayAddress()}`
    }

    setAddress(address: Address): void{
        this.address = address
    }

    displayAddress(): string {
        if (!this.address) {
            return 'No adress found'
        } else {
            return `Address: ${this.address?.street}, ${this.address?.city}, ${this.address?.postalCode}, ${this.address?.country}`
        }
    }
}