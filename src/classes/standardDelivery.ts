import { Deliverable } from "../interface/deliverable";

export class StandardDelivery implements Deliverable {

    // Reprend la méthode de l'interface Deliverable
    // Ici on "explique" la méthode pour telle classe
    estimateDeliveryTime(weight : number) : number {
        let days : number = 0;
        weight < 10 ? days = 7 : days = 10;
        return days
    }

    // Reprend la méthode de calcul des frais de livraison
    calculateShippingFee(weight: number): number {
        let fee : number = 0
        if (weight < 1) {
            fee = 5
        } else if ( weight > 1 && weight < 5 ) {
            fee = 10
        } else {
            fee = 20
        }
        return fee
    }
}