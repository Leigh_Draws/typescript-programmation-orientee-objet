import { Clothing } from "./classes/clothing";
import { Address, Customer } from "./classes/customer"
import { ExpressDelivery } from "./classes/expressDelivery";
import { Order } from "./classes/order";
import { Product } from "./classes/product"
import { Dimensions } from "./classes/product"
import { Shoes } from "./classes/shoes";
import { StandardDelivery } from "./classes/standardDelivery";
import { ClothingSize, ShoeSize } from "./enum/enum";

// Création des dimensions de l'instance culotte
let culotteDimensions: Dimensions = {
    length: 10,
    width: 5,
    height: 2
};

// Création des dimensions de l'instance Jambon
let jambonDimension: Dimensions = {
    length: 10,
    width: 5,
    height: 2
};

// Création de l'adresse de l'instance Pepper
let pepperAdress: Address = {
    street: "221B Baker Street",
    city: "London",
    postalCode: "QZS 23",
    country: "England"
}

// Création des instances
let pepper = new Customer(1, 'Pepper', 'pepperdu31@miaoumail.com')
let culotte = new Clothing(1, 'Culotte coeur', 12.3, 5, culotteDimensions, ClothingSize.XS)
let jambon = new Product(2, 'Jambon', 3.6, 12, jambonDimension)
let sabot = new Shoes(3, 'Sabots', 5, 30,
    {
        length: 10,
        width: 5,
        height: 2
    },
    ShoeSize.S39);

// Création de l'instance de Order
let firstOrder = new Order(1, pepper, [culotte, jambon],
    {
        day: 5,
        month: 12,
        year: 2021
    })

// console.log(pepper.setAddress(pepperAdress))
let standard = new StandardDelivery
let express = new ExpressDelivery

console.log(firstOrder.displayOrder())
// console.log(firstOrder.calculateShippingCost())

